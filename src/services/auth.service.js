import axios from 'axios';
// import Cookies from 'js-cookie';
const API_URL = 'http://localhost:5000/api/auth/';

class AuthService {
    login(user) {
        return axios.post(API_URL + 'signin', {
            username: user.username, password: user.password
        })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                    // Cookies.set('user', JSON.stringify(response.data));
                }
                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
        // Cookies.remove('user');
    }

    register(user) {
        return axios.post(API_URL + 'signup', {
            username: user.username,
            email: user.email,
            password: user.password
        })
    }
}

export default new AuthService();