// import Cookies from "js-cookie";

export default function authHeader() {
    // let user = JSON.parse(Cookies.get('user'));
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.accessToken) {
        return { 'x-access-token': user.accessToken };
    } else {
        return {};
    }
}